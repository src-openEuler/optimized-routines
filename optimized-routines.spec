Name:	optimized-routines
Version:	24.01
Release:	3
Summary:	Optimized implementations of various library functions for ARM architecture processors.
License:	MIT
URL:	https://github.com/ARM-software/optimized-routines
Source0:	%{name}-%{version}.tar.gz
ExclusiveArch:	aarch64
BuildRequires:	make,gcc,mpfr-devel,libmpc-devel,gmp-devel,glibc-static

%description
Optimized implementations of various library functions for ARM architecture processors.

#skip debuginfo packages
%global debug_package %{nil}

%prep
%autosetup -p1
cp config.mk.dist config.mk
sed -i "s/SUBS = math string networking/SUBS = math string/g" config.mk

%build
%make_build

%install
%make_install libdir=%{_libdir}
strip %{buildroot}%{_libdir}/lib*.so*

%check
make check

%files
%license LICENSE
%doc README
%{_includedir}/*
%{_libdir}/*

%changelog
* Tue Mar 5 2024 xiongzhou <xiongzhou4@huawei.com> - 24.01-3
- Type:Update
- ID:NA
- SUG:NA
- DESC:Update version 24.01 .

* Tue Mar 7 2023 xiongzhou <xiongzhou4@huawei.com> - 21.02-2
- Type:Bugfix
- ID:NA
- SUG:NA
- DESC:Add strip operation to shared objects.

* Sat Sep 3 2022 xiongzhou <xiongzhou4@huawei.com> - 21.02-1
- Type:Init
- ID:NA
- SUG:NA
- DESC:Init optimized-routines repository
